import React from 'react';
import { Link } from 'react-router';

class Header extends React.Component {
	
	constructor(props) {
	  super(props);
	
	  this.state = {
	  	isNavOpen: false
	  }
	}

	buttonClicked = () => {
		console.log('kliklendi');
		this.setState({
			isNavOpen: !this.state.isNavOpen
		})
	}

	render() {
		return (
			<header>
				<nav className="container navbar navbar-expand-lg navbar-light justify-content-between">				
					<a className="navbar-brand" href="#">Logo</a>

					<button className="navbar-toggler" type="button" onClick={this.buttonClicked}>
					    <span className="navbar-toggler-icon"></span>
					</button>

					<div className="navbar-collapse" style={{display: this.state.isNavOpen ? 'block' : 'none'}}>
						<ul className="navbar-nav">
					      <li className="nav-item">
					        <Link to="/" activeClassName="active" className="nav-link">Ana səhifə</Link>
					      </li>
					      <li className="nav-item">
					        <Link to="/haqqimizda" activeClassName="active" className="nav-link">Haqqımızda</Link>
					      </li>
					      <li className="nav-item">
					        <Link to="/elaqe" activeClassName="active" className="nav-link">Əlaqə</Link>
					      </li>
					    </ul>
				    </div>
			    </nav>
			</header>
		)
	}
}


export default Header;